//
//  StatusViewController.swift
//  AmsterdamStandard
//
//  Created by Tomasz Kopycki on 19/04/16.
//  Copyright © 2016 Amsterdam Standard. All rights reserved.
//

import UIKit

private let StatusVCNibName = "StatusViewController"
private let tm = TransitionManager()

class StatusViewController: UIViewController {
    
    
    // Adjustable
    var loadingColor: UIColor = .grayColor()
    var successColor: UIColor = UIColor(red: 119.0/255.0, green: 190.0/255.0, blue: 119.0/255.0, alpha: 1.0)
    var failureColor: UIColor = UIColor(red: 194.0/255.0, green: 59.0/255.0, blue: 34.0/255.0, alpha: 1.0)
    var font = UIFont.systemFontOfSize(16.0)
    var backgroundColor = UIColor.lightGrayColor() {
        willSet {
            view.backgroundColor = newValue
        }
    }
    
    var shouldUpdateSize: Bool = true
    
    private var statusMessage: String! {
        didSet {
            lblStatus.text = statusMessage
            updateSize()
        }
    }
    
    private let width = floor(UIScreen.mainScreen().bounds.size.width * 0.8)
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        transitioningDelegate = tm
        modalPresentationStyle = .Custom
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    convenience init(defaultNib: Bool, nibName: String? = nil) {
        
        if defaultNib {
            self.init(nibName: StatusVCNibName, bundle: nil)
        } else {
            self.init(nibName: nibName, bundle: nil)
        }
    }
    
    
    @IBOutlet private weak var statusIndicator: StatusIndicator!
    @IBOutlet private weak var lblStatus: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        lblStatus.font = font
        statusIndicator.strokeColor = loadingColor
        lblStatus.textColor = loadingColor
        view.backgroundColor = backgroundColor
        statusMessage = ""
    }

    func changeStatusTo(status: ProgressStatus, message: String? = nil,
                        progress: Float? = nil,
                        didFinishAnimating: StatusIndicatorBlock? = nil) {
        
        if message != nil {
            statusMessage = message
        }
        
        if let progress = progress {
            statusIndicator.progress = progress
        }
        
        switch status {
        case .Loading:
            statusIndicator.startLoading()
            statusIndicator.strokeColor = loadingColor
            lblStatus.textColor = loadingColor
        case .Failure:
            statusIndicator.completeLoading(false, completion: didFinishAnimating)
            statusIndicator.strokeColor = failureColor
            lblStatus.textColor = failureColor
        case .Success:
            statusIndicator.completeLoading(true, completion: didFinishAnimating)
            statusIndicator.strokeColor = successColor
            lblStatus.textColor = successColor
        default: break
        }
        
    }
    
    func updateSize() {
        
        guard shouldUpdateSize else { return }
        
        let size: CGSize = statusMessage
            .boundingRectWithSize(CGSizeMake(width - 20, 2000),
                                  options:NSStringDrawingOptions.UsesLineFragmentOrigin,
                                  attributes: [NSFontAttributeName: font],
                                  context: nil).size as CGSize
        
        UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.5,
                                   initialSpringVelocity: 0.8, options: [], animations: {
                                    
            self.view.frame.size = CGSize(width: self.width, height: 30 + 100 + size.height)
                                    
            }, completion: nil)
    }

}
